/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "configreader.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QTextStream>


void ConfigReader::loadConfigFile(QString fileName)
{
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly))
    {
    }

    QTextStream fileTextStream(&file);
    configRoot = new QJsonObject(
                QJsonDocument::fromJson(
                    fileTextStream.readAll().toUtf8()).object());
}

ConfigReader *ConfigReader::getInstance()
{
    static ConfigReader *instance = new ConfigReader();

    return instance;
}

QJsonObject *ConfigReader::getConfigRoot() const
{
    return configRoot;
}

QList<QString> ConfigReader::links()
{
    QList<QString> result;
    QJsonArray linksArray = configRoot->value("links").toArray();

    for (int i = 0; i < linksArray.count(); i++)
    {
        result.append(linksArray.at(i).toString());
    }

    return result;
}

QString ConfigReader::getApiKey() const
{
    return apiKey;
}

void ConfigReader::setApiKey(const QString &value)
{
    apiKey = value;
}
