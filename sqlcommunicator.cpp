/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "sqlcommunicator.h"
#include <QSqlDriver>
#include <QSqlError>
#include <QDebug>


SqlCommunicator::SqlCommunicator(QString databaseName, QString userName, QString password)
{
    this->databaseName = databaseName;
    this->userName = userName;
    this->password = password;

}

SqlCommunicator::SqlCommunicator(const SqlCommunicator &communicator)
    : databaseName(communicator.databaseName)
    , userName(communicator.userName)
    , password(communicator.password)
    , database(communicator.database)
{

}

void SqlCommunicator::connectWithDB()
{
    this->database = new QSqlDatabase();
    *(this->database) = QSqlDatabase::addDatabase("QPSQL");

    this->database->setConnectOptions();

    this->database->setHostName("localhost");
    this->database->setDatabaseName(databaseName);
    this->database->setUserName(userName);
    this->database->setPassword(password);
    this->database->setPort(5432);

    if (this->database->open()){
        qDebug() << "Opened database";
    } else {
        qDebug() << "Failed to open database";
    }
}

void SqlCommunicator::closeConnection()
{
    this->database->close();
}

QSqlError SqlCommunicator::lastError()
{
    return this->database->lastError();
}
