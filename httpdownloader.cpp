/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "httpdownloader.h"

#include "httpdownloader.h"
#include "htmlregexparser.h"
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QEventLoop>
#include <QUrl>
#include <QThread>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QSqlError>
#include <QTime>
#include "configreader.h"

static ConfigReader *conf = ConfigReader::getInstance();

HttpDownloader::HttpDownloader()
{
}

QString HttpDownloader::getHtml(QString pageAddress)
{
    QNetworkAccessManager manager;
    QNetworkRequest request = QNetworkRequest(QUrl(pageAddress));
    QNetworkReply *response = manager.get(request);
    QEventLoop event;

    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();

    return QString(response->readAll());
}

QString HttpDownloader::getJson(QString pageAddress)
{
    QNetworkAccessManager manager;
    QNetworkRequest request = QNetworkRequest(QUrl(pageAddress));
    request.setRawHeader(QByteArray("Authorization"),
                         QByteArray(QString("Token %1").arg(conf->getApiKey()).toLatin1()));
    QNetworkReply *response = manager.get(request);
    QEventLoop event;

    connect(response,SIGNAL(finished()),&event,SLOT(quit()));
    event.exec();

    return QString(response->readAll());
}

void HttpDownloader::downloadComplementaryRecords(DataCollectingCommunicator &communicator)
{
    HtmlRegexParser parser;
    const QString apiAddressPattern = "http://enduhub.com/pl/api/search/?name=%1";
    const QString pageAddressPattern = "%1?page=%2";
    const QString namePattern = "(<td class='imie_nazwisko'><a([^>]*)>([^<]*)</a></td>)";
    const QString placePattern = "(<td class='msc'>([^<]*)</td>)";
    const QString nrPattern = "(<td class='nr'>([^<]*)</td>)";
    const QString contentFilter = "(<[^>]*>)";

    for (const QString &link : conf->links())
    {
        int i = 1;
        QStringList allNames, recentNames, currentNames, places,
                numbers;

        while (true)
        {
            QString page = "";
            QString pageAddress = QString(pageAddressPattern).arg(link).arg(i);
            while (page == "")
            {
                page = getHtml(pageAddress);
            }

            parser.getPatternOccurrences(page, QRegExp(namePattern));
            parser.filterItems(QRegExp(contentFilter));
            currentNames = parser.getItems();


            if (recentNames == currentNames)
            {
                break;
            }
            allNames.append(currentNames);
            parser.getPatternOccurrences(page, QRegExp(placePattern));
            parser.filterItems(QRegExp(contentFilter));
            places.append(parser.getItems());

            parser.getPatternOccurrences(page, QRegExp(nrPattern));
            parser.filterItems(QRegExp(contentFilter));
            numbers.append(parser.getItems());
            recentNames = currentNames;
            qDebug() << QString("Downloaded names from page %1. Event url = '%2'")
                        .arg(i).arg(pageAddress);
            i++;
        }

        for (int k = 0; k < allNames.count(); k++)
        {
            const QString name = allNames[k];

            QString noSpaceName = name;

            QString apiAddress =
                    QString(apiAddressPattern).arg(noSpaceName.replace(" ", "+"));
            QString raw = "";
            while (raw == "")
            {
                raw = getJson(apiAddress);
            }

            QStringList records = constructTuplesFromJson(raw);
            communicator.appendToDB(records, "records", false);
            qDebug() << QString("%1: Downloaded records for person '%2'. Event url = '%3'")
                        .arg(k+1).arg(name).arg(link);
        }
    }
}

QStringList HttpDownloader::constructTuplesFromJson(QString &rawJson)
{
    QStringList tuples;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(rawJson.toUtf8());
    QJsonArray records = jsonDocument.array();

    for (QJsonValueRef record : records)
    {
        QJsonObject r = record.toObject();

        QString event_name = r.value(QString("event_name")).toString();
        QString event_date = r.value(QString("event_date")).toString();
        QString distance = r.value(QString("distance")).toString();
        int sport = r.value(QString("sport")).toInt();
        QString name = r.value(QString("name")).toString();
        int yob = r.value(QString("yob")).toInt();
        int nr = r.value(QString("nr")).toInt();
        QString result = r.value(QString("result")).toString();
        QString netto = r.value(QString("netto")).toString();
        int male = r.value(QString("male")).toInt();
        int female = r.value(QString("female")).toInt();
        int place = r.value(QString("place")).toInt();
        int place_category = r.value(QString("place_category")).toInt();
        int best_place = r.value(QString("best_place")).toInt();

        if (yob != 0 && sport == 2)
        {
            QString tuple =
                    QString("$$%1$$,$$%2$$,%3,$$%4$$,%5,%6,$$%7$$,$$%8$$,%9,%10,%11,%12,%13")
                    .arg(event_name)
                    .arg(event_date)
                    .arg(distance)
                    .arg(name)
                    .arg(yob)
                    .arg(nr)
                    .arg(result)
                    .arg(netto)
                    .arg(male)
                    .arg(female)
                    .arg(place)
                    .arg(place_category)
                    .arg(best_place);

            tuples.append(tuple);
        }
    }

    return tuples;
}
