/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include <QtSql/QSqlQuery>
#include <QSqlError>
#include <QtDebug>
#include <QStringBuilder>
#include <QVariant>
#include "datacollectingcommunicator.h"


DataCollectingCommunicator::DataCollectingCommunicator(QString databaseName, QString userName, QString password)
    : SqlCommunicator(databaseName, userName, password)
{

}

DataCollectingCommunicator::DataCollectingCommunicator(const SqlCommunicator &communicator)
    : SqlCommunicator(communicator)
{

}

void DataCollectingCommunicator::appendToDB(QStringList strings, QString tableName,
                                            bool stringsInQuotes)
{
    if (!strings.isEmpty())
    {
        QString insertQuery = constructInsertQuery(strings, tableName,
                                                   stringsInQuotes);
        this->database->exec(insertQuery);
    }
}

QString DataCollectingCommunicator::constructInsertQuery(QStringList strings,
                                                         QString tableName,
                                                         bool stringsInQuotes)
{
    QStringList insertQuery;

    insertQuery += QString("insert into %1 values ").arg(tableName);

    QStringList::iterator it = strings.begin();
    if (stringsInQuotes)
    {
        insertQuery += QString("('%1') ").arg(*it);
    }
    else
    {
        insertQuery += QString("(%1) ").arg(*it);
    }

    while (++it != strings.end())
    {
        if (stringsInQuotes)
        {
            insertQuery += QString(",('%1') ").arg(*it);
        }
        else
        {
            insertQuery += QString(",(%1) ").arg(*it);
        }
    }

    insertQuery += "on conflict do nothing ";
    return insertQuery.join("");
}
