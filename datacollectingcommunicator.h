/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef DATACOLLECTINGCOMMUNICATOR_H
#define DATACOLLECTINGCOMMUNICATOR_H

#include "sqlcommunicator.h"

class DataCollectingCommunicator : public SqlCommunicator
{
public:
    DataCollectingCommunicator(QString databaseName, QString userName, QString password);
    DataCollectingCommunicator(const SqlCommunicator &communicator);

    void appendToDB(QStringList strings, QString tableName,
                    bool stringsInQuotes=true);


private:
    QString constructInsertQuery(QStringList strings, QString tableName,
                                 bool stringsInQuotes=true);
};

#endif // DATACOLLECTINGCOMMUNICATOR_H
