How to compile and run:

1. Install PostgreSQL 10.
2. Create postgres user 'analyst' with password 'analyst1' and database 'enduhub_names' ('analyst' must be owner of 'enduhub_names'), or use an existing postgres user and/or database (but object "database" in config.json must be properly changed).
3. Import database from dump (link at the bottom).
4. Install QT 5.10 with charts module
5. Import project to Qt Creator (open RecordsDownloader.pro)
6. Select Desktop Qt 5.10 GCC kit
7. Add to 'links' array (config.json file) some direct URLs to pages on http://enduhub.com/ with results of event 
8. Build and run project
9. After downloading all records, run script tables.psql to create better database schema


Dump link:

- https://www.dropbox.com/s/v652c24rxr1wg3a/raw_records.psql?dl=0
