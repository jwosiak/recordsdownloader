/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include "htmlregexparser.h"

#include <QDebug>

HtmlRegexParser::HtmlRegexParser()
{

}

void HtmlRegexParser::getPatternOccurrences(const QString &htmlPage, QRegExp pattern)
{
    QStringList list;
    int pos = 0;

    while ((pos = pattern.indexIn(htmlPage, pos)) != -1) {
        list << pattern.cap(1);
        pos += pattern.matchedLength();
    }

    this->items = list;
}

void HtmlRegexParser::filterItems(QRegExp filter)
{
    this->items.replaceInStrings(filter, "");
}

QStringList HtmlRegexParser::getItems()
{
    return this->items;
}
