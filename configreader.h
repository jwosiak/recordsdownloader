/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#ifndef CONFIGREADER_H
#define CONFIGREADER_H

class QJsonObject;
class QString;

#include <QList>

class ConfigReader
{
public:
    void loadConfigFile(QString fileName);

    static ConfigReader *getInstance();
    QJsonObject *getConfigRoot() const;
    QList<QString> links();
    QString getApiKey() const;
    void setApiKey(const QString &value);

private:
    QJsonObject *configRoot;
    QString apiKey = "355a33aa741fb6a8d12d9ed9d3adbf6fe4722303";
};

#endif // CONFIGREADER_H
