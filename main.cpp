/****************************************************************************
**
** Copyright (C) 2018  Jakub Wosiak
** Contact: jakubwosiak@gmail.com
**
** This program is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>
**
****************************************************************************/

#include <QCoreApplication>
#include "configreader.h"
#include "sqlcommunicator.h"
#include "QSqlDatabase"
#include <QJsonValue>
#include <QJsonObject>
#include "httpdownloader.h"

int main(int argc, char *argv[])
{
    ConfigReader *conf = ConfigReader::getInstance();

    if (argc > 1)
    {
        conf->loadConfigFile(argv[1]);
    }
    else
    {
        conf->loadConfigFile("config.json");
    }

    if (argc > 2)
    {
        conf->setApiKey(argv[2]);
    }

    QJsonObject *doc = conf->getConfigRoot();
    QJsonObject db = doc->value(QString("database")).toObject();

    SqlCommunicator *communicator =
            new SqlCommunicator(db.value(QString("name")).toString(),
                                db.value(QString("user")).toString(),
                                db.value(QString("password")).toString());
    communicator->connectWithDB();

    QCoreApplication a(argc, argv);

    HttpDownloader down;
    DataCollectingCommunicator com(*communicator);
    down.downloadComplementaryRecords(com);
    a.exit(0);
    return 0;
}
